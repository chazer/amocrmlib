<?php
/**
 * IContactsSetRequest.php
 *
 * @author: chazer
 * @created: 25.09.14 13:40
 */

namespace chazer\amocrmlib\api\interfaces\contacts\requests;

use chazer\amocrmlib\api\interfaces\contacts\IListAddItem;
use chazer\amocrmlib\api\interfaces\contacts\IListUpdateItem;
use chazer\amocrmlib\api\interfaces\ISetRequest;

/**
 * Interface IContactsSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\api\interfaces\contacts\requests
 */
interface IContactsSetRequest extends ISetRequest
{
    public function add(IListAddItem $item);

    public function update(IListUpdateItem $item);
}