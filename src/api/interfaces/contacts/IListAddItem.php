<?php
/**
 * IListAddItem.php
 *
 * @author: chazer
 * @created: 25.09.14 13:40
 */

namespace chazer\amocrmlib\api\interfaces\contacts;

use chazer\amocrmlib\api\interfaces\fields\ICustomField;
use chazer\amocrmlib\api\interfaces\ICommonListAddItem;
use DateTime;

interface IListAddItem extends ICommonListAddItem
{
    /**
     * Имя контакта
     * @return string
     */
    public function getName();

    /**
     * Уникальный идентификатор записи в клиентской программе
     * @return null|int
     */
    public function getRequestId();

    /**
     * Дата создания этого контакта
     * @return null|DateTime
     */
    public function getDateCreate();

    /**
     * Дата последнего изменения этого контакта (не обязательный параметр)
     * @return null|DateTime
     */
    public function getLastModified();

    /**
     * Уникальный идентификатор ответственного пользователя
     * @return null|int
     */
    public function getResponsibleUserId();

    /**
     * @param $id int linked lead id
     */
    public function linkLead($id);

    /**
     * Список id связанных сделок
     * @return null|array
     */
    public function getLinkedLeadsId();

    /**
     * Имя компании
     * @return null|string
     */
    public function getCompanyName();

    /**
     * Добавить доп. поле
     * @param ICustomField $item
     */
    public function addCustomField(ICustomField $item);

    /**
     * Дополнительные поля контакта
     * @return null|array
     */
    public function getCustomFields();

    /**
     * Названия тегов через запятую
     * @return null|string
     */
    public function getTags();
}