<?php
/**
 * ListUpdateItem.php
 *
 * @author: chazer
 * @created: 23.09.14 12:19
 */

namespace chazer\amocrmlib\api\interfaces\tasks;

use chazer\amocrmlib\api\interfaces\ICommonListUpdateItem;

interface IListUpdateItem extends ICommonListUpdateItem
{
    public function getId();

    public function getElementId();

    public function getElementType();

    public function getDateCreate();

    public function getLastModified();

    public function getRequestId();

    public function getTaskType();

    public function getText();

    public function getResponsibleUserId();

    public function getCompleteTill();
}