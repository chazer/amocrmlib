<?php
/**
 * ITasksListRequest.php
 *
 * @author: chazer
 * @created: 23.09.14 13:45
 */

namespace chazer\amocrmlib\api\interfaces\tasks\requests;

use chazer\amocrmlib\api\interfaces\IRequest;

interface ITasksListRequest extends IRequest
{
    public function getIfModifiedSince();

    public function getType();

    public function getLimitRows();

    public function getLimitOffset();

    public function getId();

    public function getResponsibleUserId();
} 