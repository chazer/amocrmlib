<?php
/**
 * ISetResponse.php
 *
 * @author: chazer
 * @created: 29.09.14 14:58
 */

namespace chazer\amocrmlib\api\interfaces;
use chazer\amocrmlib\classes\EntityId;

interface ISetResponse extends IResponse
{
    /**
     * @return EntityId[]
     */
    public function getAddedList();

    public function getNotUpdatedList();
} 