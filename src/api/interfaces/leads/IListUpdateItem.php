<?php
/**
 * IListUpdateItem.php
 *
 * @author: chazer
 * @created: 26.09.14 18:48
 */

namespace chazer\amocrmlib\api\interfaces\leads;

use chazer\amocrmlib\api\interfaces\fields\ICustomField;
use chazer\amocrmlib\api\interfaces\ICommonListUpdateItem;
use DateTime;

interface IListUpdateItem extends ICommonListUpdateItem
{
    /**
     * @return int Уникальный идентификатор сделки, который указывается с целью его обновления
     */
    public function getId();

    /**
     * Название сделки
     * @return string
     */
    public function getName();

    /**
     * Дата создания текущей сделки
     * @return null|DateTime
     */
    public function getDateCreate();

    /**
     * Дата последнего изменения данной сущности, если параметр не указан, или он меньше чем имеющийся в БД,
     * то обновление не произойдет и в ответ придет информация из Базы Данных amoCRM
     * @return DateTime
     */
    public function getLastModified();

    /**
     * Статус сделки
     * @return int
     */
    public function getStatusId();

    /**
     * Бюджет сделки
     * @return null|int
     */
    public function getPrice();

    /**
     * Уникальный идентификатор ответственного пользователя
     * @return null|int
     */
    public function getResponsibleUserId();

    /**
     * Уникальный идентификатор записи в клиентской программе (не обязательный параметр)
     * (Информация о request_id нигде не сохраняется)
     * @return null|int
     */
    public function getRequestId();

    /**
     * Добавить доп. поле
     * @param ICustomField $item
     */
    public function addCustomField(ICustomField $item);

    /**
     * Дополнительные поля
     * @return null|array
     */
    public function getCustomFields();

    /**
     * Названия тегов через запятую
     * @return null|string
     */
    public function getTags();

    /**
     * ID связываемой компании
     * @return mixed
     */
    public function getLinkedCompanyId();
} 