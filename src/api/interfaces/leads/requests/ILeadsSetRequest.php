<?php
/**
 * ILeadsSetRequest.php
 *
 * @author: chazer
 * @created: 26.09.14 18:37
 */

namespace chazer\amocrmlib\api\interfaces\leads\requests;

use chazer\amocrmlib\api\interfaces\ISetRequest;
use chazer\amocrmlib\api\interfaces\leads\IListAddItem;
use chazer\amocrmlib\api\interfaces\leads\IListUpdateItem;

/**
 * Interface ILeadsSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\api\interfaces\leads\requests
 */
interface ILeadsSetRequest extends ISetRequest
{
    public function add(IListAddItem $item);

    public function update(IListUpdateItem $item);
}