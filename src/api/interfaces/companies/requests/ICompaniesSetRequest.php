<?php
/**
 * ICompaniesSetRequest.php
 *
 * @author: chazer
 * @created: 15.10.14 17:46
 */

namespace chazer\amocrmlib\api\interfaces\companies\requests;

use chazer\amocrmlib\api\interfaces\companies\IListAddItem;
use chazer\amocrmlib\api\interfaces\companies\IListUpdateItem;
use chazer\amocrmlib\api\interfaces\ISetRequest;

/**
 * Interface ICompaniesSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\api\interfaces\companies\requests
 */
interface ICompaniesSetRequest extends ISetRequest
{
    public function add(IListAddItem $item);

    public function update(IListUpdateItem $item);
} 