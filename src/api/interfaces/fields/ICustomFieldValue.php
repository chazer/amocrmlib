<?php
/**
 * ICustomFieldValue.php
 *
 * @author: chazer
 * @created: 25.09.14 15:28
 */

namespace chazer\amocrmlib\api\interfaces\fields;

interface ICustomFieldValue
{
    /**
     * Значение дополнительного поля
     * @return string
     */
    public function getValue();

    /**
     * Выбираемый тип дополнительного поля (напр телефон домашний, рабочий и т д)
     * @return string
     */
    public function getEnum();
} 