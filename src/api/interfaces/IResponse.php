<?php
/**
 * IResponse.php
 *
 * @author: chazer
 * @created: 29.09.14 12:57
 */

namespace chazer\amocrmlib\api\interfaces;

interface IResponse
{
    /**
     * @param $accInfo
     */
    public function setAccountInfo($accInfo);

    /**
     * @param IRequest $request
     * @return void
     */
    public function setRequestObject($request);

    /**
     * @param Array $data
     */
    public function setData($data);

    public function getServerTime();
} 