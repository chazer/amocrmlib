<?php
/**
 * TaskElementTypes.php
 *
 * @author: chazer
 * @created: 23.09.14 21:44
 */

namespace chazer\amocrmlib\api\types;

class TaskElementTypes
{
    const CONTACT = 1;
    const LEAD = 2;
} 