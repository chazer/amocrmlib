<?php
/**
 * leads.php
 *
 * @author: chazer
 * @created: 26.09.14 20:56
 */

use chazer\amocrmlib\classes\fields\CustomField;
use chazer\amocrmlib\classes\leads\ListAddItem;
use chazer\amocrmlib\classes\leads\ListUpdateItem;
use chazer\amocrmlib\classes\leads\requests\LeadsSetRequest;
use chazer\amocrmlib\classes\Schemes;
use chazer\amocrmlib\classes\Types;

Schemes::addScheme(
    ListAddItem::getSchemeName(),
    [
        // Название сделки
        'name' => [
            'type' => Types::String,
            'require' => true,
        ],
        // Дата создания текущей сделки (не обязательный параметр)
        'date_create' => [
            'type' => Types::Timestamp,
        ],
        // Дата изменения текущей сделки (не обязательный параметр)
        'last_modified' => [
            'type' => Types::Timestamp,
        ],
        // Статус сделки (статусы см. Информация аккаунта -
        // https://developers.amocrm.ru/rest_api/accounts_current.php )
        'status_id' => [
            'type' => Types::Number,
            'require' => true,
        ],
        // Бюджет сделки
        'price' => [
            'type' => Types::Number,
        ],
        // Уникальный идентификатор ответственного пользователя(пользователи см. Информация аккаунта -
        // https://developers.amocrm.ru/rest_api/accounts_current.php )
        'responsible_user_id' => [
            'type' => Types::Number,
        ],
        // Уникальный идентификатор записи в клиентской программе (не обязательный параметр)
        'request_id' => [
            'type' => Types::Number,
        ],
        // Дополнительные поля сделки
        'custom_fields' => [
            'type' => Types::ObjectsList(CustomField::getSchemeName())
        ],
        // Названия тегов через запятую
        'tags' => [
            'type' => Types::String,
        ],
        // Связь с компанией
        'linked_company_id' => [
            'type' => Types::Number,
        ]
    ]
);

Schemes::addScheme(
    ListUpdateItem::getSchemeName(),
    Schemes::merge(
        Schemes::getScheme(ListAddItem::getSchemeName()),
        [
            // Уникальный идентификатор сделки, который указывается с целью его обновления
            'id' => [
                'require' => true,
                'type' => Types::Number,
            ],
            'name' => [
                'require' => false,
            ],
            'status_id' => [
                'require' => false,
            ],
            // Дата последнего изменения данной сущности, если параметр не указан, или он меньше чем имеющийся в БД,
            // то обновление не произойдет и в ответ придет информация из Базы Данных amoCRM
            // (Является обязательным параметром)
            'last_modified' => [
                'require' => true,
                'type' => Types::Timestamp
            ],
        ]
    )
);

Schemes::addScheme(
    LeadsSetRequest::getSchemeName(),
    [
        // Список добавляемых сделок
        'add' => [
            'type' => Types::ObjectsList(ListAddItem::getClassName())
        ],
        // Обновление существующей сделки
        'update' => [
            'type' => Types::ObjectsList(ListUpdateItem::getClassName())
        ],
    ]
);
