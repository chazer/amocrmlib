<?php
/**
 * fields.php
 *
 * @author: chazer
 * @created: 26.09.14 21:03
 */

// Значение доп поля, и, если нужно, дополнительный тип (для полей типа “мультисписок” просто перечисляем id выбранных значений)
use chazer\amocrmlib\classes\fields\CustomField;
use chazer\amocrmlib\classes\fields\CustomFieldValue;
use chazer\amocrmlib\classes\Schemes;
use chazer\amocrmlib\classes\Types;

Schemes::addScheme(
    CustomFieldValue::getSchemeName(),
    [
        // Значение дополнительного поля
        'value' => [
            'type' => Types::String,
        ],
        // Выбираемый тип дополнительного поля (напр телефон домашний, рабочий и т д)
        'enum' => [
            'type' => Types::String,
        ]
    ]
);

// Описание доп. поля
Schemes::addScheme(
    CustomField::getSchemeName(),
    [
        // Уникальный идентификатор заполняемого дополнительного поля (см. Информация аккаунта -
        // https://developers.amocrm.ru/rest_api/accounts_current.php )
        'id' => [
            'type' => Types::Number,
        ],
        // Значения
        'values' => [
            'type' => Types::ObjectsList(CustomFieldValue::getSchemeName())
        ]
    ]
);