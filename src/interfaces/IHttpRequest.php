<?php
/**
 * IHttpRequest.php
 *
 * @author: chazer
 * @created: 24.09.14 1:28
 */

namespace chazer\amocrmlib\interfaces;

interface IHttpRequest
{
    public function reset();

    /**
     * @param $url
     * @param string $method
     * @param null|array $post
     * @param null|array $headers
     * @return bool
     */
    public function query($url, $method = 'GET', $post = null, $headers = null);

    /**
     * @return int
     */
    public function getResponseCode();

    /**
     * @return string
     */
    public function getResponseBody();

    /**
     * @param $value int seconds
     */
    public function setTimeoutConnect($value);

    /**
     * @param $value int seconds
     */
    public function setTimeoutExecute($value);

    /**
     * @param $value bool
     */
    public function enableFollowLocation($value);

    /**
     * @param $value bool
     */
    public function enableKeepCookies($enable);

    /**
     * @return string
     */
    public function getUserAgent();

    /**
     * @param $value string
     */
    public function setUserAgent($value);

    /**
     * @return array
     */
    public function getCookies();

    /**
     * @param $array array cookies list in format: 'name' => 'value'
     */
    public function setCookies($array);

    /**
     * @return null|string
     */
    public function getResponseContentType();

    /**
     * @return array
     */
    public function getDefaultHeaders();

    /**
     * @param $array array headers list in format: 'name' => 'value'
     * @param $replace bool remove others headers
     */
    public function setDefaultHeaders($array, $replace = false);

    /**
     * @return bool
     */
    public function hasErrors();

    /**
     * @return array error messages list
     */
    public function getErrors();
} 