<?php
/**
 * Schemes.php
 *
 * @author: chazer
 * @created: 23.09.14 1:20
 */

namespace chazer\amocrmlib\classes;

class Schemes
{
    private static $schemes = [];

    private static function normalizeSchemeElement($value)
    {
        $default = [
            'require' => false,
            'type' => null,
        ];
        return array_merge($default, $value);
    }

    private static function parseSchemeElement($value, &$require, &$types)
    {
        $require = (bool) $value['require'];
        $types = $value['type'];
        $types = $types ? (is_array($types) ? $types : [$types]) : [];
    }

    private static function normalizeScheme($scheme)
    {
        if (is_string($scheme)) {
            $scheme = self::getScheme($scheme);
        }
        if (!is_array($scheme)) {
            throw new \Exception('Wrong scheme value');
        }
        return $scheme;
    }

    private static function matchTypes($value, $types, &$match = null)
    {
        foreach ($types as $t) {
            if (self::validateValue($value, $t)) {
                $match = $t;
                return true;
            }
        }
        return false;
    }

    private static function getObjectProperty($object, $property)
    {
        $method = 'get'.str_replace(['_', '-'], '', ucwords($property));
        if (method_exists($object, $method)) {
            $value = $object->$method();
        }
        elseif (property_exists($object, $property)) {
            $value = $object->$property;
        }
        else {
            throw new \Exception("Wrong object property '$property'");
        }
        return $value;
    }

    private static function validateValue($value, Types $type)
    {
        return $type->validateValue($value);
    }

    private static function serializeValue(Types $type, $value)
    {
        return $type->serializeValue($value);
    }

    /**
     * @param $name
     * @param $scheme
     */
    public static function addScheme($name, $scheme)
    {
        $name = strtolower($name);
        foreach ($scheme as $k => $v) {
            if (isset($v['type']) && !($v['type'] instanceof Types)) {
                // Initialize types
                $new = null;
                $temp = $type = $v['type'];
                try {
                    if (is_array($type)) {
                        $list = [];
                        foreach($type as $j=>$st) {
                            $temp = $st;
                            $list[$j] = new Types($st);
                        }
                        $new = $list;
                    } else {
                        $new = new Types($type);
                    }
                } catch (\UnexpectedValueException $e) {
                    throw new \Exception("Wrong type '$temp'", 0, $e);
                }
                if ($new) {
                    $scheme[$k]['type'] = $new;
                }
            }
        }
        self::$schemes[$name] = $scheme;
    }

    /**
     * @param $name
     * @return bool
     */
    public static function hasScheme($name)
    {
        $name = strtolower($name);
        return isset(self::$schemes[$name]);
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public static function getScheme($name)
    {
        $name = strtolower($name);
        if (!isset(self::$schemes[$name])) {
            throw new \Exception('Scheme not found');
        }
        return self::$schemes[$name];
    }

    /**
     * @param mixed $item
     * @param array|string $scheme scheme or scheme name
     * @param bool $strict
     * @return bool
     * @throws \Exception
     */
    public static function validateObject($item, $scheme, $strict = true)
    {
        $scheme = self::normalizeScheme($scheme);

        foreach ($scheme as $property=>$si) {
            $si = self::normalizeSchemeElement($si);
            self::parseSchemeElement($si, $req, $types);

            $value = self::getObjectProperty($item, $property);
            $ok = true;

            if ($types && !empty($value)) {
                $ok = $ok && self::matchTypes($value, $types);
            }

            if ($req && empty($value)) {
                if ($strict)
                    throw new \Exception("Property '$property' is required");
                return false;
            }

            if (!$ok) {
                if ($strict)
                    throw new \Exception('Object test error' . " on property '$property'");
                return false;
            }
        }
        return true;
    }

    /**
     * @param mixed $item
     * @param array|string $scheme scheme or scheme name
     * @return array
     */
    public static function serializeObject($item, $scheme)
    {
        $scheme = self::normalizeScheme($scheme);
        $out = [];

        foreach ($scheme as $property=>$si) {
            $si = self::normalizeSchemeElement($si);
            self::parseSchemeElement($si, $req, $types);

            $value = self::getObjectProperty($item, $property);

            if ($types && !is_null($value)) {
                $matchType = null;
                self::matchTypes($value, $types, $matchType);
                if ($matchType) {
                    $data = self::serializeValue($matchType, $value);
                    if (is_object($data) || $matchType == Types::ObjectsList) {
                        $list = is_array($data) ? $data : [$data];
                        $mod = [];
                        foreach($list as $li) {
                            $objSchemes = self::searchSchemesForObject($li);
                            if ($objScheme = array_shift($objSchemes)) {
                                $mod[] = self::serializeObject($li, $objScheme);
                            }
                        }
                        $data = $mod;
                    }
                    $out[$property] = $data;
                }
            }

//            if (!$req && empty($value)) {
//                // Nothing
//            }
        }
        return $out;
    }

    /**
     * @param $object
     * @return array
     */
    public static function searchSchemesForObject($object)
    {
        $interfaces = class_implements($object, false);
        $exclude = [];
        foreach ($interfaces as $a)
            if (!Schemes::hasScheme($a)) {
                $exclude[] = $a;
            } else {
                foreach ($interfaces as $b) {
                    if (!in_array($b, $exclude) && $a !== $b && is_a($a, $b)) {
                        $exclude[] = $a;
                    }
                }
            }
        $list = array_diff($interfaces, $exclude);
        $schemes = [];
        foreach ($list as $l) {
            $schemes[] = Schemes::getScheme($l);
        }
        return $schemes;
    }

    /**
     * @param Array $a First scheme data
     * @param Array $b,... Others scheme data
     * @return array
     */
    public static function merge($a, $b)
    {
        $args=func_get_args();
        $res=array_shift($args);
        while(!empty($args))
        {
            $next=array_shift($args);
            foreach($next as $k => $v)
            {
                if(is_integer($k))
                    isset($res[$k]) ? $res[]=$v : $res[$k]=$v;
                elseif(is_array($v) && isset($res[$k]) && is_array($res[$k]))
                    $res[$k]=self::merge($res[$k],$v);
                else
                    $res[$k]=$v;
            }
        }
        return $res;
    }
} 