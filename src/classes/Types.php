<?php
/**
 * Types.php
 *
 * @author: chazer
 * @created: 23.09.14 0:40
 */
namespace chazer\amocrmlib\classes;

class Types extends \SplEnum
{
    const Number = 'number';
    const String = 'string';
    const Timestamp = 'timestamp';
    const Object = 'object';
    const Enum = 'enum';
    const ObjectsList = 'objects_list';
    const ValuesList = 'values_list';

    private $className = null;
    private $enumValues;
    private $min = null;
    private $max = null;

    /** @var Types */
    private $subType;

    public static function Number($min = null, $max = null)
    {
        $t = new Types(Types::Number);
        $t->min = $min;
        $t->max = $max;
        return $t;
    }

    public static function Object($class = null)
    {
        $t = new Types(Types::Object);
        $t->className = $class;
        return $t;
    }

    public static function Enum($values = null)
    {
        $t = new Types(Types::Enum);
        $t->enumValues = $values;
        return $t;
    }

    public static function ObjectsList($class = null)
    {
        $t = new Types(Types::ObjectsList);
        $t->className = $class;
        return $t;
    }

    public static function ValuesList($type = null)
    {
        $t = new Types(Types::ValuesList);
        if ($type) {
            $t->subType = ($type instanceof Types) ? $type : new Types($type);
        }
        return $t;
    }

    public function validateValue($value)
    {
        if ($this == Types::String) {
            return is_string($value) || ($value == (string) $value);
        }
        if ($this == Types::Number) {
            return
                is_numeric($value) &&
                (is_null($this->min) || $this->min <= $value) &&
                (is_null($this->max) || $this->max >= $value);
        }
        if ($this == Types::Enum) {
            return is_array($this->enumValues) ? in_array($value, $this->enumValues) : is_int($value);
        }
        if ($this == Types::Timestamp) {
            return ($value instanceof \DateTime);
            //return is_numeric($value) && date('U', $value) == $value;
        }
        if ($this == Types::Object) {
            return ($this->className) && is_a($value, $this->className);
        }
        if ($this == Types::ObjectsList) {
            if ($ok = is_array($value) ){
                foreach ($value as $v) {
                    if ($ok = $ok && is_a($v, $this->className))
                        continue;
                    break;
                }
            }
            return $ok;
        }
        if ($this == Types::ValuesList) {
            if ($ok = is_array($value) ){
                if ($this->subType) {
                    foreach ($value as $v) {
                        if ($ok = $ok && $this->subType->validateValue($v))
                            continue;
                        break;
                    }
                }
            }
            return $ok;
        }
        return false;
    }

    public function serializeValue($value)
    {
        if ($this == Types::String) {
            return (string) $value;
        }
        if ($this == Types::Number) {
            return (int) $value;
        }
        if ($this == Types::Timestamp) {
            return ($value instanceof \DateTime) ? $value->getTimestamp() : 0;
        }
        if ($this == Types::ValuesList) {
            if ($this->subType instanceof Types) {
                $out = [];
                foreach ($value as $k=>$v) {
                    $out[$k] = $this->subType->serializeValue($v);
                }
                return $out;
            }
        }
        return $value;
    }
}