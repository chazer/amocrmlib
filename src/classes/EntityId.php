<?php
/**
 * EntityId.php
 *
 * @author: chazer
 * @created: 30.09.14 12:30
 */

namespace chazer\amocrmlib\classes;

class EntityId
{
    /**
     * @var int Уникальный идентификатор новой сущности
     */
    public $id;

    /**
     * @var int Уникальный идентификатор сущности в клиентской программе, если request_id не передан в запросе,
     * то он генерируется автоматически
     */
    public $request_id;

    public $server_time;

    public function __construct($params = null)
    {
        if ($params && is_array($params)) {
            $this->id = isset($params['id']) ? $params['id'] : null;
            $this->request_id = isset($params['request_id']) ? $params['request_id'] : null;
            $this->server_time = isset($params['server_time']) ? $params['server_time'] : null;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }

    public function getServerTime()
    {
        return $this->server_time;
    }
} 