<?php
/**
 * ListUpdateItem.php
 *
 * @author: chazer
 * @created: 26.09.14 20:40
 */

namespace chazer\amocrmlib\classes\leads;

use chazer\amocrmlib\api\interfaces\fields\ICustomField;
use chazer\amocrmlib\api\interfaces\leads\IListUpdateItem;
use chazer\amocrmlib\classes\SchemeObject;
use DateTime;

class ListUpdateItem extends SchemeObject implements IListUpdateItem
{
    /**
     * @var int Уникальный идентификатор сделки, который указывается с целью его обновления
     */
    public $id;

    /**
     * @var string [require] Название сделки
     */
    public $name;

    /**
     * @var DateTime Дата создания текущей сделки (не обязательный параметр)
     */
    public $date_create;

    /**
     * @var DateTime [require] Дата изменения текущей сделки
     */
    public $last_modified;

    /**
     * @var int [require] Статус сделки (статусы см. Информация аккаунта -
     * https://developers.amocrm.ru/rest_api/accounts_current.php )
     */
    public $status_id;

    /**
     * @var int Бюджет сделки
     */
    public $price;

    /**
     * @var int Уникальный идентификатор ответственного пользователя (пользователи см. Информация аккаунта -
     * https://developers.amocrm.ru/rest_api/accounts_current.php )
     */
    public $responsible_user_id;

    /**
     * @var int Уникальный идентификатор записи в клиентской программе (не обязательный параметр)
     */
    public $request_id;

    /**
     * @var array Дополнительные поля сделки
     */
    public $custom_fields;

    /**
     * @var string Названия тегов через запятую
     */
    public $tags;

    /**
     * @var int ID связываемой компании
     */
    public $linked_company_id;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function getLastModified()
    {
        return $this->last_modified;
    }

    public function getStatusId()
    {
        return $this->status_id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getResponsibleUserId()
    {
        return $this->responsible_user_id;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }

    public function addCustomField(ICustomField $item)
    {
        if (!is_array($this->custom_fields))
            $this->custom_fields = [];
        $this->custom_fields[] = $item;
    }

    public function getCustomFields()
    {
        return $this->custom_fields;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getLinkedCompanyId()
    {
        return $this->linked_company_id;
    }
} 