<?php
/**
 * CompaniesSetRequest.php
 *
 * @author: chazer
 * @created: 15.10.14 17:44
 */

namespace chazer\amocrmlib\classes\companies\requests;

use chazer\amocrmlib\api\interfaces\companies\IListAddItem;
use chazer\amocrmlib\api\interfaces\companies\IListUpdateItem;
use chazer\amocrmlib\api\interfaces\companies\requests\ICompaniesSetRequest;
use chazer\amocrmlib\classes\companies\ListAddItem;
use chazer\amocrmlib\classes\companies\ListUpdateItem;
use chazer\amocrmlib\classes\CustomSetRequest;
use chazer\amocrmlib\classes\Schemes;

/**
 * Class CompaniesSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\classes\companies\requests
 */
class CompaniesSetRequest extends CustomSetRequest implements ICompaniesSetRequest
{
    public function add(IListAddItem $item)
    {
        Schemes::validateObject($item, ListAddItem::getSchemeName());
        $this->listAdd[] = $item;
    }

    public function update(IListUpdateItem $item)
    {
        Schemes::validateObject($item, ListUpdateItem::getSchemeName());
        $this->listUpdate[] = $item;
    }
} 