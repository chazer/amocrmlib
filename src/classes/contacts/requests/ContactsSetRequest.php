<?php
/**
 * ContactsSetRequest.php
 *
 * @author: chazer
 * @created: 25.09.14 14:42
 */

namespace chazer\amocrmlib\classes\contacts\requests;

use chazer\amocrmlib\api\interfaces\contacts\IListAddItem;
use chazer\amocrmlib\api\interfaces\contacts\IListUpdateItem;
use chazer\amocrmlib\api\interfaces\contacts\requests\IContactsSetRequest;
use chazer\amocrmlib\classes\contacts\ListAddItem;
use chazer\amocrmlib\classes\contacts\ListUpdateItem;
use chazer\amocrmlib\classes\CustomSetRequest;
use chazer\amocrmlib\classes\Schemes;

/**
 * Class ContactsSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\classes\contacts\requests
 */
class ContactsSetRequest extends CustomSetRequest implements IContactsSetRequest
{
    public function add(IListAddItem $item)
    {
        Schemes::validateObject($item, ListAddItem::getSchemeName());
        $this->listAdd[] = $item;
    }

    public function update(IListUpdateItem $item)
    {
        Schemes::validateObject($item, ListUpdateItem::getSchemeName());
        $this->listUpdate[] = $item;
    }
}
