<?php
/**
 * ListAddItem.php
 *
 * @author: chazer
 * @created: 23.09.14 12:57
 */

namespace chazer\amocrmlib\classes\tasks;

use chazer\amocrmlib\api\interfaces\tasks\IListAddItem;
use chazer\amocrmlib\classes\SchemeObject;
use DateTime;

class ListAddItem extends SchemeObject implements IListAddItem
{
    /**
     * @var int [require] Уникальный идентификатор контакта или сделки (сделка или контакт указывается в element_type)
     */
    public $element_id;

    /**
     * @var int [require] Тип привязываемого елемента (1 - контакт, 2 - сделка)
     */
    public $element_type;

    /**
     * @var DateTime Timestamp. Дата создания данной задачи (не обязательный параметр)
     */
    public $date_create;

    /**
     * @var int Дата последнего изменения данной задачи (не обязательный параметр)
     */
    public $last_modified;

    /**
     * @var int Внешний идентификатор записи (не обязательный параметр)(Информация о request_id нигде не сохраняется,
     * лишь передается обратно в ответе)
     */
    public $request_id;

    /**
     * @var int [require] Тип задачи (типы задач см. Информация аккаунта -
     * https://developers.amocrm.ru/rest_api/accounts_current.php )
     */
    public $task_type;

    /**
     * @var string [require] Текст задачи
     */
    public $text;

    /**
     * @var int Уникальный идентификатор ответственного пользователя(пользователи см. Информация аккаунта -
     * https://developers.amocrm.ru/rest_api/accounts_current.php )
     */
    public $responsible_user_id;

    /**
     * @var DateTime [require] Timestamp. Дата до которой необходимо завершить задачу. Если указано время 23:59,
     * то в интерфейсах системы вместо времени будет отображаться "Весь день".
     */
    public $complete_till;

    public function getElementId()
    {
        return $this->element_id;
    }

    public function getElementType()
    {
        return $this->element_type;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function getLastModified()
    {
        return $this->last_modified;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }

    public function getTaskType()
    {
        return $this->task_type;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getResponsibleUserId()
    {
        return $this->responsible_user_id;
    }

    public function getCompleteTill()
    {
        return $this->complete_till;
    }
}
