<?php
/**
 * TasksSetResponse.php
 *
 * @author: chazer
 * @created: 23.09.14 12:16
 */

namespace chazer\amocrmlib\classes\tasks\responses;

class TasksSetResponse
{
    public $id;
    public $request_id;
    public $server_time;
}
