AmoCRM API library
=========

API for amocrm.ru

Version
----

0.0.1

Links
-----------

* [amocrm] - amoCRM site.
* [devdocs] - amoCRM developer documentation.

Installation
--------------

```sh
composer require "chazer/amocrmlib:*"
```

Usage
----

TODO


License
----

TODO

Requirement
----

PHP 5.3.0 or greater;

[amocrm]:https://amocrm.ru
[devdocs]:https://developers.amocrm.ru
